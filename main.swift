//
//  main.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit

let appDelegateClass: AnyClass = NSClassFromString("UnitTestingAppDelegate") ?? AppDelegate.self

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(appDelegateClass))
