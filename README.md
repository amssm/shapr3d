# Shapr3D app

### Requirments:
- [x] The user should be able to import .shapr files for conversion into the app.
- [x] These files can be converted to .step , .stl and .obj formats.
- [x] The imported files should be displayed in a list.
- [x] Every item should contain the already available conversion as
labels and a progressbar if there is a conversion in progress.
- [x] A detailed view can be opened per item which displays the thumbnail in a bigger resolution.
- [x] A detailed view can be opened per item which displays the active conversion progress.
- [x] A detailed view can be opened per item which displays size, both for the input file.
- [ ] Every item should display a
thumbnail about the file.
- [ ] A detailed view can be opened per item which displays size, both for the output files.
- [x] Use a mock implementation in a form of a
separate library.
- [x] Define the interface for this conversion library so that is supports the most
efficient way of passing input and output for the particular platform.
- [x] Imported and converted files should be persisted between separate runs.
- [x] Allow multiple conversions in parallel and at the same time respect device and OS capabilities.

### More Details:
1. Used MVP as an archetcute along with ***Navigator*** which is responsible for handling navigation between views.
2. THere are some requirements which I was not able to finish as I have a feature that should be released very soon to 4.7 Million users so apologies but I have very limited time.
3. There are some use cases that should be handled in a better way and I'm aware of that.
4. Usually When I work on any project from scratch, I use my open-source [Library](https://github.com/ahmadmssm/iOS_Bootstrap) to save some time but I preferred to do everything without it this time.

### Used this library:
1. RxSwift -> Swift package manager.
2. [Resolver](https://github.com/ahmadmssm/iOS_Bootstrap), A DI library which I'm contributing to, I Used it to apply dependency injection.
3. [SwiftEventBus](https://github.com/cesarferreira/SwiftEventBus), An elegant layer written on top of Notification center.
4. [EmptyDataSet](https://github.com/Xiaoye220/EmptyDataSet-Swift), An Elegant library to handle Empty table view in a nice way.

Notes: 
1. I have created a button that creates dummy files, please use it to test the app.
2. All the mentioned libraries are included in the project except RxSwift.
3. For me, It was the first time to deal with Local files and File manager, So many thanks for this opportunity.
4. If I had more time, I would have added units tests.

### Screenshots :
![](https://gitlab.com/amssm/shapr3d/-/raw/master/FilesListingWithProgressOne.png "Files list with conversion in progress").
![](https://gitlab.com/amssm/shapr3d/-/raw/master/FilesListingWithProgressTwo.png "Files list with conversion in progress").
![](https://gitlab.com/amssm/shapr3d/-/raw/master/FileDetailsWithProgress.png "File details with progress").
