//
//  HomePresenter.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

import RxSwift
import Foundation
import Shapr3DFileConversionFrameWork

class HomePresenter: BasePresenter<HomeView> {
    
    private var homeRepo: HomeRepo!
    private var disposeBag: DisposeBag!
    private var appFileManager: AppFileManager!

    required convenience init(viewDelegate: HomeView,
                              homeRepo: HomeRepo,
                              disposeBag: DisposeBag,
                              appFileManager: AppFileManager) {
        self.init(viewDelegate: viewDelegate)
        self.homeRepo = homeRepo
        self.disposeBag = disposeBag
        self.appFileManager = appFileManager
    }
    
    func createDummyFiles() {
        self.appFileManager
            .createThreeDummyFiles()
            .observeOn(RxSchedulers.ui)
            .subscribe { [weak self] in
                self?.getViewDelegate()?.didCreateDummyFiles()
            } onError: { [weak self] error in
                self?.getViewDelegate()?.didFailToConvertFile(error: error)
            }
            .disposed(by: self.disposeBag)
    }
    
    func saveAndConvertImportedFileTo(type: FileType, fileURL: URL)  {
        homeRepo
            .saveImportedFile(fileURL: fileURL)
            .flatMapCompletable({ [weak self] fileId in
                guard let strongSelf = self else {
                    let error = AppError(message: "unknown_error".localized())
                    return Completable.error(error)
                }
                return strongSelf.appFileManager.convertFileAndSaveOutput(fileId: fileId, fileURL: fileURL, fileType: type)
            })
            .observeOn(RxSchedulers.ui)
            .subscribe { [weak self] in
                self?.getViewDelegate()?.didConvertFile()
            } onError: { [weak self] error in
                self?.getViewDelegate()?.didFailToConvertFile(error: error)
            }
            .disposed(by: self.disposeBag)
    }
    
    deinit {
        disposeBag = nil
    }
}
