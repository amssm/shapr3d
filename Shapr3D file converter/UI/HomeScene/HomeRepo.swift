//
//  HomeRepo.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

import RxSwift
import Foundation

class HomeRepo {
    
    private let filesHistoryTable: FilesHistoryTable
    
    init(filesHistoryTable: FilesHistoryTable) {
        self.filesHistoryTable = filesHistoryTable
    }
    
    func saveImportedFile(fileURL: URL) -> Single<Int> {
        return self.filesHistoryTable
            .insetNewDocument(document: Document(fileURL: fileURL))
            .subscribeOn(RxSchedulers.background)
            .observeOn(RxSchedulers.ui)
    }
}
