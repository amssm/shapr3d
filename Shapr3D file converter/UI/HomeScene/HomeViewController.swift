//
//  HomeViewController.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit
import MobileCoreServices

class HomeViewController: BaseViewController<HomePresenter, HomeView>, HomeView, UIDocumentPickerDelegate {

    @IBOutlet private weak var createDummyFilesButton: UIButton!
    @IBOutlet private weak var openFilesPickerButton: UIButton!
    @IBOutlet private weak var openRecentFilesButton: UIButton!
    @LazyInjected private var navigator: Navigator
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    override func initUI() {
        self.title = "home".localized()
        self.createDummyFilesButton.layer.cornerRadius = 15
        self.openFilesPickerButton.layer.cornerRadius = 15
        self.openRecentFilesButton.layer.cornerRadius = 15
        self.createDummyFilesButton.setTitle("create_dummy_files".localized(), for: .normal)
        self.openFilesPickerButton.setTitle("pick_file".localized(), for: .normal)
        self.openRecentFilesButton.setTitle("show_history".localized(), for: .normal)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        // The first document that was picked.
        guard let sourceURL = urls.first else { return }
        self.dialogs?.openConversionTypesAlert(from: self, action: { fileType in
            self.presenter?.saveAndConvertImportedFileTo(type: fileType, fileURL: sourceURL)
        })
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func didCreateDummyFiles() {
        
    }
    
    func didConvertFile() {
        dialogs?.showDialog(viewController: self, message: "conversion_success".localized())
    }
    
    func didFailToConvertFile(error: Error) {
        dialogs?.showDialog(viewController: self, message: "conversion_failed".localized())
    }
    
    @IBAction private func createDummyFilesButtonAction(_ sender: UIButton) {
        presenter?.createDummyFiles()
    }
    
    @IBAction private func openFilesPickerButtonAction(_ sender: UIButton) {
        navigator.openFilesPicker(from: self)
    }
    
    @IBAction private func openRecentFilesButtonAction(_ sender: UIButton) {
        navigator.openImportedFilesView(from: self)
    }
}
