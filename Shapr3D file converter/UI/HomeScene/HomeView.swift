//
//  HomeView.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

protocol HomeView {
    func didCreateDummyFiles()
    func didConvertFile()
    func didFailToConvertFile(error: Error)
}
