//
//  ImportedFilesView.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

protocol ImportedFilesView {
    func didGetEmptyImportedFiles()
    func didGetImportedFiles(files: [HistoryFile])
    func didFailToGetImportedFiles(errorMessage: String)
}
