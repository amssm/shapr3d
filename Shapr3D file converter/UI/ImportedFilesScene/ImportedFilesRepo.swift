//
//  ImportedFilesRepo.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

import RxSwift
import Shapr3DFileConversionFrameWork

class ImportedFilesRepo {
    
    private let appFileManager: AppFileManager
    private let filesHistoryTable: FilesHistoryTable
    private lazy var convertedFiles: [Document] = {
        appFileManager.getAllConvertedDocuments() ?? []
    }()
    
    init(appFileManager: AppFileManager, filesHistoryTable: FilesHistoryTable) {
        self.appFileManager = appFileManager
        self.filesHistoryTable = filesHistoryTable
    }
    
    final func getAllImportedFiles() -> Single<[HistoryFile]> {
        return filesHistoryTable
            .fetchAll()
            .map({ [weak self] importedFiles -> [HistoryFile] in
                guard let strongSelf = self else {
                    return importedFiles
                }
                if(!strongSelf.convertedFiles.isEmpty) {
                    importedFiles.forEach ({ importedFile in
                        importedFile.conversionProcess = strongSelf.getConversionProcessIfExists(by: importedFile.id)
                        strongSelf.convertedFiles.forEach { document in
                            for fileType in FileType.allCases {
                                if(document.fileName == importedFile.name && document.fileExtension == fileType.ext) {
                                    importedFile.availableConversions.append(fileType)
                                }
                            }
                        }
                    })
                }
                return importedFiles
            })
            .subscribeOn(RxSchedulers.background)
            .observeOn(RxSchedulers.ui)
    }
    
    private final func getConversionProcessIfExists(by fileId: Int) -> ConversionProcess? {
        FileConversionService.shared.getConversionProcessIfExists(by: fileId)
    }
}
