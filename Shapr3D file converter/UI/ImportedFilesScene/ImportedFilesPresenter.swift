//
//  ImportedFilesPresenter.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

import RxSwift

class ImportedFilesPresenter: BasePresenter<ImportedFilesView> {

    private var disposeBag: DisposeBag!
    private var importedFilesRepo: ImportedFilesRepo!
    
    required convenience init(viewDelegate: ImportedFilesView,
                              disposeBag: DisposeBag,
                              repo: ImportedFilesRepo) {
        self.init(viewDelegate: viewDelegate)
        self.disposeBag = disposeBag
        self.importedFilesRepo = repo
    }
    
    override func viewControllerDidLoad() {
        self.fetchAllImportedFiles()
        // Listen to conversion updates
        FileConversionService.subscribeToConversionBroadcastEvent { _ in
            self.fetchAllImportedFiles()
        }
    }
    
    private func fetchAllImportedFiles() {
        importedFilesRepo
            .getAllImportedFiles()
            .subscribe { [weak self] importedFiles in
                if(importedFiles.isEmpty) {
                    self?.getViewDelegate()?.didGetEmptyImportedFiles()
                }
                else {
                    self?.getViewDelegate()?.didGetImportedFiles(files: importedFiles)
                }
            } onError: { [weak self] error in
                self?.getViewDelegate()?.didFailToGetImportedFiles(errorMessage: error.localizedDescription)
            }
            .disposed(by: disposeBag)
    }
    
    deinit {
        disposeBag = nil
    }
}
