//
//  ImportedFileTableViewCell.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit
import Shapr3DFileConversionFrameWork

class ImportedFileTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var fileName: UILabel!
    @IBOutlet private weak var progressView: UIProgressView!
    @IBOutlet private weak var stepTypeLabel: UILabel!
    @IBOutlet private weak var stlTypeLabel: UILabel!
    @IBOutlet private weak var objTypeLabel: UILabel!
    
    func configureCell(with file: HistoryFile) {
        self.selectionStyle = .none
        self.fileName.text = file.fullName
        if let conversionProcess = file.conversionProcess {
            if(conversionProcess.progress < 1) {
                self.progressView.isHidden = false
                self.progressView.progress = conversionProcess.progress
            }
            else {
                self.progressView.isHidden = true
            }
        }
        else {
            self.progressView.isHidden = true
        }
        if(!file.availableConversions.contains(FileType.obj)) {
            self.objTypeLabel.isHidden = true
        }
        else {
            self.configureConversionTypeLabel(label: self.objTypeLabel, text: "obj", color: .systemBlue)

        }
        if(!file.availableConversions.contains(FileType.stl)) {
            self.stlTypeLabel.isHidden = true

        }
        else {
            self.configureConversionTypeLabel(label: self.stlTypeLabel, text: "stl", color: .systemGreen)

        }
        if(!file.availableConversions.contains(FileType.step)) {
            self.stepTypeLabel.isHidden = true
        }
        else {
            self.configureConversionTypeLabel(label: self.stepTypeLabel, text: "step", color: .systemRed)
        }
    }
    
    private func configureConversionTypeLabel(label: UILabel, text: String, color: UIColor) {
        label.text = text.localized()
        label.backgroundColor = color
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
    }
}
