//
//  ImportedFilesTableViewAdapterDelegate.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 28/11/2020.
//

protocol ImportedFilesTableViewAdapterDelegate {
    func didClickFile(file: HistoryFile)
}
