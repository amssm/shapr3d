//
//  ImportedFilesTableViewAdapter.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit

class ImportedFilesTableViewAdapter: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private let tableView: UITableView
    private var dataSource: [HistoryFile] = []
    private let delegate: ImportedFilesTableViewAdapterDelegate
    private lazy var emptyViewAttributes: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.systemBlue,
            .paragraphStyle: paragraphStyle
        ]
        return attributes
    }()
    
    init(tableView: UITableView, delegate: ImportedFilesTableViewAdapterDelegate) {
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        self.configureTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ImportedFileTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configureCell(with: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didClickFile(file: self.dataSource[indexPath.row])
    }
    
    func postAndReloadTable(list: [HistoryFile]) {
        if(list.isEmpty) {
            self.tableView.separatorStyle = .none
        }
        else {
            self.tableView.separatorStyle = .singleLine
        }
        self.dataSource.removeAll()
        self.dataSource.append(contentsOf: list)
        self.tableView.reloadData()
    }
    
    private func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.registerNib(ImportedFileTableViewCell.self)
    }
}

extension ImportedFilesTableViewAdapter: EmptyDataSetSource, EmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return #imageLiteral(resourceName: "empty_files")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let text = "no_imported_files".localized()
        return NSAttributedString(string: text, attributes: emptyViewAttributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let text = "no_imported_files_info".localized()
        return NSAttributedString(string: text, attributes: emptyViewAttributes)
    }
}
