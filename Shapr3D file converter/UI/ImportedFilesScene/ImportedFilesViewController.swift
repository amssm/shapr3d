//
//  ImportedFilesViewController.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit

class ImportedFilesViewController: BaseViewController<ImportedFilesPresenter, ImportedFilesView>, ImportedFilesView, ImportedFilesTableViewAdapterDelegate {

    @IBOutlet private weak var tableView: UITableView!
    @LazyInjected var navigator: Navigator
    private lazy var tableViewAdapter: ImportedFilesTableViewAdapter? = {
        ImportedFilesTableViewAdapter(tableView: self.tableView, delegate: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initUI() {
        self.title = "imported_file".localized()
    }
    
    func didClickFile(file: HistoryFile) {
        self.navigator.openImportedFileDetailsView(from: self, file: file)
    }
    
    func didGetEmptyImportedFiles() {
        self.tableViewAdapter?.postAndReloadTable(list: [])
    }
    
    func didGetImportedFiles(files: [HistoryFile]) {
        self.tableViewAdapter?.postAndReloadTable(list: files)
    }
    
    func didFailToGetImportedFiles(errorMessage: String) {
        dialogs?.showDialog(viewController: self, title: "error".localized(), message: errorMessage)
    }
}
