//
//  DocumentViewController.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit

class DocumentViewController: UIViewController {

    @IBOutlet private weak var fileThumbnail: UIImageView!
    @IBOutlet private weak var fileNameLabel: UILabel!
    @IBOutlet private weak var fileNameValueLabel: UILabel!
    @IBOutlet private weak var fileSizeLabel: UILabel!
    @IBOutlet private weak var fileSizeValueLabel: UILabel!
    @IBOutlet private weak var conversionProgressView: UIProgressView!
    //
    private var file: HistoryFile!
    
    convenience init(file: HistoryFile) {
        self.init()
        self.file = file
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fileThumbnail.image = #imageLiteral(resourceName: "file")
        self.title = "file_details".localized()
        self.fileNameLabel.text = "file_name".localized()
        self.fileNameValueLabel.text = self.file.fullName
        self.fileSizeLabel.text = "file_size".localized()
        self.fileSizeValueLabel.text = self.file.size
        self.conversionProgressView.isHidden = true
        // Listen to conversion updates
        FileConversionService.subscribeToConversionBroadcastEvent { progress in
            self.conversionProgressView.progress = progress
            if(progress <= 1) {
                self.conversionProgressView.isHidden = false
            }
            else {
                self.conversionProgressView.isHidden = true
            }
        }
    }
}
