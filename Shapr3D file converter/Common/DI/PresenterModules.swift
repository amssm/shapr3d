//
//  PresenterModules.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

extension Resolver {
    static func presenterModules() {
        register { (resolver, args) -> HomePresenter in
            let viewDelegate = args as! HomeView
            return HomePresenter(viewDelegate: viewDelegate, homeRepo: resolve(), disposeBag: resolve(), appFileManager: resolve())
        }
        register { (resolver, args) -> ImportedFilesPresenter in
            let viewDelegate = args as! ImportedFilesView
            return ImportedFilesPresenter(viewDelegate: viewDelegate, disposeBag: resolve(), repo: resolve())
        }
    }
}
