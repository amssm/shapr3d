//
//  OtherModules.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

import UIKit
import RxSwift

extension Resolver {
    static func otherModules() {
        register { Dialogs() }
        register { DisposeBag() }
        register { AppFileManager() }
        // Singleton
        register { Navigator() }.scope(application)
    }
}
