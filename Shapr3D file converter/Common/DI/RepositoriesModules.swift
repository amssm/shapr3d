//
//  RepositoriesModules.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

extension Resolver {
    static func repositoriesModules() {
        register { HomeRepo(filesHistoryTable: resolve()) }
        register { ImportedFilesRepo(appFileManager: resolve(), filesHistoryTable: resolve()) }
    }
}
