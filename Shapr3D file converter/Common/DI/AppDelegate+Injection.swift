//
//  AppDelegate+Injection.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit

//  Ref: https://github.com/hmlongco/Resolver/blob/master/Documentation/Registration.md
extension Resolver: ResolverRegistering {
    
    public static func registerAllServices() {
        Resolver.defaultScope = Resolver.unique
        otherModules()
        presenterModules()
        repositoriesModules()
        offlineCachingModules()
    }
}
