//
//  OfflineCachingModules.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

extension Resolver {
    static func offlineCachingModules() {
        // Singleton
        register { FilesHistoryTable() }.scope(application)
    }
}
