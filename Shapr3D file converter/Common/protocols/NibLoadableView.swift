//
//  NibLoadableView.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

protocol NibLoadableView {
    static var nibName: String { get }
}

extension NibLoadableView  {
    static var nibName: String {
        return String(describing: self)
    }
}
