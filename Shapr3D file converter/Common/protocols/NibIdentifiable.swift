//
//  NibIdentifiable.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

protocol NibIdentifiable: class {
    static var reuseIdentifier: String { get }
}

extension NibIdentifiable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
