//
//  BaseViewDelegate.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit

protocol BaseViewDelegate where Self: UIViewController {}
