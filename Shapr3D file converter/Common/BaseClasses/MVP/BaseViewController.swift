//
//  BaseViewController.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit

class BaseViewController<P, V>: UIViewController, BaseViewDelegate, Resolving where P: BasePresenter<V> {

    @OptionalInjected var dialogs: Dialogs?

    lazy var presenter: P? = { resolver.optional(args: self) }()

    override open func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewControllerDidLoad()
        initUI()
        presenter?.viewControllerDidFinishSettingUpUI()
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.viewControllerWillRefresh()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewControllerDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter?.viewControllerWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.viewControllerDidDisappear()
    }
    
    deinit {
        presenter = nil
    }

    func initUI () { fatalError("Must Override") }
    
    func iniPresenter () -> P { fatalError("Must Override") }
}


