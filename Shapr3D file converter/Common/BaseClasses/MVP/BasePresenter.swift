//
//  BasePresenter.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

class BasePresenter<T> {
    
    private final weak var viewDelegate: BaseViewDelegate!
    
    required public init (viewDelegate: T) {
        self.viewDelegate = viewDelegate as? BaseViewDelegate
    }
    
    public final func getViewDelegate() -> T? {
        return viewDelegate as? T
    }
    
    func viewControllerDidLoad() {}

    func viewControllerDidFinishSettingUpUI() {}

    func viewControllerWillRefresh() {}
        
    func viewControllerDidAppear() {}

    func viewControllerWillDisappear() {}

    func viewControllerDidDisappear() {}

    deinit {
        if (viewDelegate != nil) { viewDelegate = nil }
    }
}

