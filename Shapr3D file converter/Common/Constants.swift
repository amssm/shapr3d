//
//  Constants.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 21/11/2020.
//

import UIKit

typealias PickerOpener = UIViewController & UIDocumentPickerDelegate
