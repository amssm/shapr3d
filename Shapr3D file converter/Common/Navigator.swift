//
//  Navigator.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit

class Navigator {
      
    private lazy var appWindow: UIWindow = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.window!
    }()
    
    func startInitialViewController() {
        let mainViewController = HomeViewController()
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let navigationController = UINavigationController(rootViewController: mainViewController)
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.barTintColor = .systemBlue
        navigationController.navigationBar.titleTextAttributes = textAttributes
        UIView.transition(
            with: appWindow,
            duration: 0.3,
            options: .transitionFlipFromLeft,
            animations: {
                self.appWindow.rootViewController = navigationController
                self.appWindow.makeKeyAndVisible()
        }, completion: nil)
    }
    
    final func openFilesPicker(from viewController: PickerOpener) {
        let filesPicker = UIDocumentPickerViewController(documentTypes: ["com.ams.shapr"], in: .import)
        filesPicker.delegate = viewController
        filesPicker.allowsMultipleSelection = false
        filesPicker.modalPresentationStyle = .formSheet
        viewController.present(filesPicker, animated: true, completion: nil)
    }
    
    final func openImportedFilesView(from viewController: PickerOpener) {
        let destinationViewController = ImportedFilesViewController()
        viewController.navigationController?.pushViewController(destinationViewController, animated: true)
    }
   
    final func openImportedFileDetailsView(from viewController: UIViewController, file: HistoryFile) {
        let destinationViewController = DocumentViewController(file: file)
        viewController.navigationController?.pushViewController(destinationViewController, animated: true)
    }
}
