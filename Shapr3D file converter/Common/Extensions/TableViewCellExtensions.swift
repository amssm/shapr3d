//
//  TableViewCellExtensions.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit

extension UITableViewCell: NibIdentifiable, NibLoadableView {}
