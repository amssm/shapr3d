//
//  AppError.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 25/11/2020.
//

import Foundation

struct AppError {
  
    let message: String

    init(message: String) {
        self.message = message
    }
}

extension AppError: LocalizedError {
    var errorDescription: String? { return message }
}
