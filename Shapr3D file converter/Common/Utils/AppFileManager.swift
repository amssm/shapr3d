//
//  AppFileManager.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import RxSwift
import Foundation
import Shapr3DFileConversionFrameWork

class AppFileManager: IShaprFileConvert {
    
    private let fileManager = FileManager.default

    func converFileWith(fileURL: URL, fileType: FileType, completion: @escaping (ConversionResult) -> ()) {
        do {
            let data = try Data(contentsOf: fileURL, options: .alwaysMapped)
            completion(.Success(data))
        }
        catch {
            completion(.Error(error))
        }
    }
    
    func createThreeDummyFiles() -> Completable {
        return Completable.create { [weak self] completable in
            guard let strongSelf = self else {
                let error = AppError(message: "unknown_error".localized())
                completable(.error(error))
                    return Disposables.create {}
                }
                if let folderURL = strongSelf.getAppFolderPath() {
                    do {
                        let string = "Dummy Shapr 3D file content"
                        let urlOne = folderURL.appendingPathComponent("FirstFile.shapr")
                        let urlTwo = folderURL.appendingPathComponent("SecondFile.shapr")
                        let urlThree = folderURL.appendingPathComponent("ThirdFile.shapr")
                        let urlFourth = folderURL.appendingPathComponent("FourthFile.shapr")
                        let urlFifth = folderURL.appendingPathComponent("FifthFile.shapr")
                        //
                        try string.write(to: urlOne, atomically: true, encoding: .utf8)
                        try string.write(to: urlTwo, atomically: true, encoding: .utf8)
                        try string.write(to: urlTwo, atomically: true, encoding: .utf8)
                        try string.write(to: urlThree, atomically: true, encoding: .utf8)
                        try string.write(to: urlFourth, atomically: true, encoding: .utf8)
                        try string.write(to: urlFifth, atomically: true, encoding: .utf8)
                    }
                    catch {
                        print(error.localizedDescription)
                    }
                }
                else {
                    let error = AppError(message: "failed_to_save_output_file".localized())
                    completable(.error(error))
                }
                return Disposables.create {}
            }
    }
    
    func convertFileAndSaveOutput(fileId: Int, fileURL: URL, fileType: FileType) -> Completable {
        return self.converFileWith(fileURL: fileURL, fileType: fileType)
            .subscribeOn(RxSchedulers.background)
            .flatMapCompletable { data in
                return self.writeFile(fileId: fileId, fileURL: fileURL, fileContent: data, outputType: fileType)
            }
    }
    
    private func converFileWith(fileURL: URL, fileType: FileType) -> Single<Data> {
        return Single.create { [weak self] single in
            guard let strongSelf = self else {
                let error = AppError(message: "unknown_error".localized())
                single(.error(error))
                return Disposables.create {}
            }
            strongSelf.converFileWith(fileURL: fileURL, fileType: fileType) { result in
                switch (result) {
                case .Success(let data):
                    single(.success(data))
                case .Error(let error):
                    single(.error(error))
                }
            }
            return Disposables.create()
        }
    }
    
    private func writeFile(fileId: Int, fileURL: URL, fileContent: Data, outputType: FileType) -> Completable {
        return Completable.create { [weak self] completable in
            guard let strongSelf = self else {
                let error = AppError(message: "unknown_error".localized())
                completable(.error(error))
                return Disposables.create {}
            }
            if let folderURL = strongSelf.getConversionsFolderPath() {
                let fileNameWithExt = fileURL.lastPathComponent
                let fileName = { fileNameWithExt.components(separatedBy: ".").first }()
                let fileExtension = outputType.ext
                let outputFileURL = folderURL.appendingPathComponent("\(fileName!).\(fileExtension)")
                do {
                    try fileContent.write(to: outputFileURL)
                    DispatchQueue.main.async {
                        var count = 0
                        Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { timer in
                            count += 1
                            if count >= 12 {
                                timer.invalidate()
                                completable(.completed)
                            }
                            else {
                                _ = FileConversionService.shared.addNewOrUPdateConversionProcess(fileId: fileId, fileType: outputType)
                            }
                        }
                     }
                } catch {
                    print(error.localizedDescription)
                }
            }
            else {
                let error = AppError(message: "failed_to_save_output_file".localized())
                completable(.error(error))
            }
            return Disposables.create {}
        }
    }
    
    func getAllConvertedDocuments() -> [Document]? {
        return self.getAllConvertedFilesURLs()?.map({ url -> Document in
            Document(fileURL: url)
        })
    }
    
    private func getAllConvertedFilesURLs() -> [URL]? {
        if let folderURL = getConversionsFolderPath() {
            do {
                let filsURLs = try fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil, options: [])
                return filsURLs
            }
            catch {
                print(error)
            }
        }
       return nil
    }
    
    private func getAppFolderPath() -> URL? {
        let pathes = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let appFolderURL = pathes[0]
        return appFolderURL
    }
        
    private func getConversionsFolderPath() -> URL? {
        do {
            let conversionsFolderName = "Conversions"
            let conversionsFolder = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let folderURL = conversionsFolder.appendingPathComponent(conversionsFolderName)
            let folderExists = (try? folderURL.checkResourceIsReachable()) ?? false
            if !folderExists {
                try fileManager.createDirectory(at: folderURL, withIntermediateDirectories: false)
            }
            return folderURL
        }
        catch {
            // Failed to get or create folder
            return nil
        }
    }
}
