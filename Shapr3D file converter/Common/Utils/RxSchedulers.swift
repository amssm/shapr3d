//
//  RxSchedulers.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 24/11/2020.
//

import RxSwift

public struct RxSchedulers {
    static let ui = MainScheduler.instance
    static let background = ConcurrentDispatchQueueScheduler(qos: .background)
}
