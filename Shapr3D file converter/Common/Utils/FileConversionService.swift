//
//  FileConversionService.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 28/11/2020.
//

import Shapr3DFileConversionFrameWork

class FileConversionService {
    
    private static let conversionBroadcastKey = "conversionBroadcastKey"
    
    private var conversionProcesses: [ConversionProcess] = []
    
    static var shared: FileConversionService = {
        let instance = FileConversionService()
        return instance
    }()

    private init() {}
    
    final func addNewOrUPdateConversionProcess(fileId: Int, fileType: FileType) -> Bool {
        if(self.conversionProcesses.isEmpty || !self.hasConversion(fileId: fileId)) {
            self.conversionProcesses.append(ConversionProcess(fileId: fileId, outputFileType: fileType))
            return true
        }
        else {
            return self.updateProgress(fileId: fileId)
        }
    }
    
    private final func updateProgress(fileId: Int) -> Bool {
        if let index = conversionProcesses.firstIndex(where: { $0.fileId == fileId }) {
            let conversionProcess = self.conversionProcesses[index]
            conversionProcess.incrementProgress()
            if(conversionProcess.progress >= 1) {
                self.conversionProcesses.remove(at: index)
            }
            FileConversionService.notifyConversionProcessUpdated(progress: conversionProcess.progress)
            return true
        }
        return false
    }
    
    final func getConversionProcessIfExists(by fileId: Int) -> ConversionProcess? {
        return self.conversionProcesses.filter({ $0.fileId == fileId }).first
    }
    
    private final func hasConversion(fileId: Int) -> Bool {
        conversionProcesses.contains(where: { $0.fileId == fileId })
    }
    
    static func notifyConversionProcessUpdated(progress: Float)  {
        SwiftEventBus.post(FileConversionService.conversionBroadcastKey, sender: progress)
    }
    
    @objc
    static func subscribeToConversionBroadcastEvent(closure: @escaping (_ progress: Float) -> ())  {
        SwiftEventBus.onMainThread(self, name: FileConversionService.conversionBroadcastKey) { result in
            closure(result?.object as! Float)
        }
    }
}
