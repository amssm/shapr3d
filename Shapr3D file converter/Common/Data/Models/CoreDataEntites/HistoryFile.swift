//
//  HistoryFile.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import CoreData
import Shapr3DFileConversionFrameWork

@objc(HistoryFile)
final class HistoryFile: NSManagedObject {
    @NSManaged var id: Int
    @NSManaged var fullName: String
    @NSManaged var url: String
    @NSManaged var size: String
    //
    var conversionProcess: ConversionProcess?
    var availableConversions: [FileType] = []
    lazy var name = { fullName.components(separatedBy: ".").first }()
    lazy var ext = { fullName.components(separatedBy: ".")[1] }()
}
