//
//  ConversionProcess.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 28/11/2020.
//

import Shapr3DFileConversionFrameWork

class ConversionProcess {
    
    var fileId: Int
    var progress: Float = 0.0
    var outputFileType: FileType
    
    init(fileId: Int, outputFileType: FileType) {
        self.fileId = fileId
        self.outputFileType = outputFileType
    }
    
    func incrementProgress() {
        if(self.progress < 100) {
            self.progress += 0.1
        }
    }
}
