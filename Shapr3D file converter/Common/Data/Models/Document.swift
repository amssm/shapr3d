//
//  Document.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit

class Document: UIDocument {
    
    lazy var fullFileName = { fileURL.lastPathComponent }()
    lazy var fileURLAsString = { fileURL.absoluteString }()
    lazy var fileName = { fullFileName.components(separatedBy: ".").first }()
    lazy var fileExtension = { fullFileName.components(separatedBy: ".")[1] }()

    var state: String {
        switch documentState {
        case .normal:
            return "Normal"
        case .closed:
            return "Closed"
        case .inConflict:
            return "Conflict"
        case .savingError:
            return "Save Error"
        case .editingDisabled:
            return "Editing Disabled"
        case .progressAvailable:
            return "Progress Available"
        default:
            return "Unknown"
        }
    }
            
    override func contents(forType typeName: String) throws -> Any {
        // Encode your document with an instance of NSData or NSFileWrapper
        return Data()
    }
    
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        // Load your document from contents
    }
}
