//
//  FilesHistoryTable.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import RxSwift
import CoreData

class FilesHistoryTable: CoreDataManager<HistoryFile> {
    
    func insetNewDocument(document: Document) -> Single<Int> {
        if let itemId = self.getItemIdIfExistBy(name: document.fullFileName) {
            return Single.just(itemId)
        }
        else {
            let importedFile = createNewEntity()
            importedFile.id = self.getCurrentTimeInterval()
            importedFile.fullName = document.fullFileName
            importedFile.url = document.fileURLAsString
            importedFile.size = document.fileURL.fileSizeAsString
            return self.insertRecord(record: importedFile).andThen(Single.just(importedFile.id))
        }
    }
    
    private func getCurrentTimeInterval() -> Int {
        let currentDate = Date()
        let timeInterval = currentDate.timeIntervalSince1970
        return Int(timeInterval)
    }
    
    private func getItemIdIfExistBy(name: String) -> Int? {
        let fetchRequest = getFetchReuest()
        fetchRequest.fetchLimit =  1
        fetchRequest.predicate = NSPredicate(format: "fullName == %@" ,name)
        do {
            let fetchedItems = try self.context.fetch(fetchRequest) as! [HistoryFile]
            if (fetchedItems.count > 0) {
                return fetchedItems.first?.id
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return nil
    }
}
