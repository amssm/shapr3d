//
//  CoreDataConfigurations.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import CoreData

final class CoreDataConfigurations {
    
    static var persistentContainer: NSPersistentContainer = {
        return getPersistentContainer(withName: "AppDataBase")
    }()
    
    private static func getPersistentContainer(withName name: String) -> NSPersistentContainer {
        let container = NSPersistentContainer(name: name)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        return container
    }
}
