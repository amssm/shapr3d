//
//  CoreDataManager.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import Foundation
import UIKit
import CoreData
import RxSwift

class CoreDataManager<T: NSManagedObject> {
    
    lazy var context: NSManagedObjectContext = {
        CoreDataConfigurations.persistentContainer.viewContext
    }()
    
    func insertRecord(record: T) -> Completable {
        return Completable.create { [weak self] completable in
            do {
                self?.context.insert(record)
                try self?.context.save()
                completable(.completed)
            }
            catch {
                completable(.error(error))
            }
            return Disposables.create {}
        }
    }
    
    open func fetchAll() -> Single<[T]> {
        return Single.create { [weak self] single in
            guard let strongSelf = self else {
                let error = AppError(message: "unknown_error".localized())
                single(.error(error))
                return Disposables.create {}
            }
            var items : [T] = []
            do {
                let arrayOfItems = try strongSelf.context.fetch(strongSelf.getFetchReuest()) as! [T]
                arrayOfItems.forEach { item in items.append(item) }
                single(.success(items))
            }
            catch let error { single(.error(error)) }
            return Disposables.create()
        }
    }
    
    func deleteRecord(record: T) -> Completable {
        return Completable.create { [weak self] completable in
            self?.context.delete(record)
            completable(.completed)
            return Disposables.create {}
        }
    }
    
    func deleteAllRecords() -> Completable {
        return Completable.create { [weak self] completable in
            guard let strongSelf = self else {
                let error = AppError(message: "core_date_error".localized())
                completable(.error(error))
                return Disposables.create {}
            }
            do {
                let arrayOfItems = try strongSelf.context.fetch(strongSelf.getFetchReuest())
                arrayOfItems.forEach { record in
                    self?.context.delete(record as! NSManagedObject)
                }
                try strongSelf.context.save()
            }
            catch let error as NSError {
                completable(.error(error))
            }
            completable(.completed)
            return Disposables.create {}
        }
    }
    
    final func createNewEntity() -> T {
        return T.init(context: self.context)
    }
    
    final func getFetchReuest() -> NSFetchRequest<NSFetchRequestResult> {
        let entityName: String = T.classForCoder().description()
        return NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
    }
    
    final func getDeleteRequest() -> NSBatchDeleteRequest {
        return NSBatchDeleteRequest(fetchRequest: getFetchReuest())
    }
}
