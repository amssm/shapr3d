//
//  Dialogs.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

import UIKit
import Shapr3DFileConversionFrameWork

class Dialogs {
    
    private let message = "select_conversion_extension".localized()
    private let stlFile = FileType.stl.ext
    private let objFile = FileType.obj.ext
    private let stepFile = FileType.step.ext
    private let cancel = "cancel".localized()
    
    func openConversionTypesAlert(from vc: UIViewController, action: @escaping (_ file: FileType) -> ()) {
        let converterOptionsAlert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        converterOptionsAlert.addAction(UIAlertAction(title: stlFile, style: .default, handler: { _ in
            action(FileType.stl)
        }))
        converterOptionsAlert.addAction(UIAlertAction(title: objFile, style: .default, handler: { _ in
            action(FileType.obj)
        }))
        converterOptionsAlert.addAction(UIAlertAction(title: stepFile, style: .default, handler: { _ in
            action(FileType.step)
        }))
        converterOptionsAlert.addAction(UIAlertAction(title: cancel, style: .default, handler: { (action: UIAlertAction!) in
            converterOptionsAlert.dismiss(animated: true, completion: nil)
        }))
        vc.present(converterOptionsAlert, animated: true, completion: nil)
    }
    
    func showDialog(viewController: UIViewController, title: String = "message".localized(), message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "ok".localized(), style: .default, handler: nil))
        viewController.present(alertController, animated: true, completion: nil)
    }
}
