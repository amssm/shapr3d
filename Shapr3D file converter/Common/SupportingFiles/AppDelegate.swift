//
//  AppDelegate.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 20/11/2020.
//

import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate, Resolving {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.startInitialViewController()
        return true
    }
    
    private func startInitialViewController() {
        let navigator: Navigator = resolver.resolve()
        navigator.startInitialViewController()
    }
}


