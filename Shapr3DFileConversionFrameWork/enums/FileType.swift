//
//  FileType.swift
//  Shapr3D file converter
//
//  Created by Ahmad Mahmoud on 23/11/2020.
//

public enum FileType: String, CaseIterable {
    
    case shapr
    case step
    case stl
    case obj
    
    public var ext: String {
        return self.rawValue
    }
}
