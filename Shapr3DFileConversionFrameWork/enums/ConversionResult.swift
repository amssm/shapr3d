//
//  ConversionResult.swift
//  Shapr3DFileConversionFrameWork
//
//  Created by Ahmad Mahmoud on 26/11/2020.
//

import Foundation

public enum ConversionResult {
    case Success(Data)
    case Error(Error)
}
