//
//  IShaprFileConvert.swift
//  Shapr3DFileConversionFrameWork
//
//  Created by Ahmad Mahmoud on 26/11/2020.
//

public protocol IShaprFileConvert {
    func converFileWith(fileURL: URL, fileType: FileType, completion: @escaping (ConversionResult) -> ())
}
